package com.treeman.myguess

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var secret = Random().nextInt(10)+1
        text_secret.setText(secret.toString())

        btn_guess.setOnClickListener {

            var message = ""
            var guess = ed_guess.text.toString().toInt()

            if ( secret < guess){
                message = "smaller!!"
            }else if( secret > guess ){
                message = "bigger!!"
            }else{
                message = "you got it !! secret is $secret"
            }

            AlertDialog.Builder(this)
                .setTitle("Guess")
                .setMessage(message)
                .setPositiveButton("OK",null)
                .show()
        }


    }
}
